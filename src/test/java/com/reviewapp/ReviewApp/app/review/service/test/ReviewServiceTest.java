package com.reviewapp.ReviewApp.app.review.service.test;

import com.reviewapp.ReviewApp.api.reivew.entity.Review;
import com.reviewapp.ReviewApp.api.reivew.entity.ReviewDTO;
import com.reviewapp.ReviewApp.api.reivew.exception.ReviewNotFoundException;
import com.reviewapp.ReviewApp.api.reivew.repository.ReviewRepository;
import com.reviewapp.ReviewApp.api.reivew.service.ReviewService;
import com.reviewapp.ReviewApp.api.reivew.service.ReviewServiceImpl;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ReviewServiceTest {

    private static final int REVIEW_ID = 1;

    @InjectMocks
    private ReviewServiceImpl reviewServiceImpl;

    @Mock
    private ReviewRepository review_repo;

    @BeforeEach
    void setUp(){
        List<Review> reviews = new ArrayList<>();
        Review review = new Review();
        review.setReviewid(REVIEW_ID);
        review.setReview("test review service");
        when(review_repo.findById(anyInt())).thenReturn(Optional.of(review));
        when(review_repo.findByReviewContains(anyString())).thenReturn(reviews);

        when(reviewServiceImpl.getReviewById(99999)).thenThrow(new ReviewNotFoundException("Review id: " + 99999));
    }

    @Test
    @Order(1)
    @DisplayName("testGetReviewByReviewId")
    void testGetReviewByReviewId(){
        List<ReviewDTO> reviewExpected = new ArrayList<ReviewDTO>();
        ReviewDTO reviewDto = new ReviewDTO();
        reviewDto.setReviewid(REVIEW_ID);
        reviewDto.setReview("test review service");
        reviewDto.setReview_display("test review service");
        reviewExpected.add(reviewDto);

        List<ReviewDTO> reviewModel = reviewServiceImpl.getReviewById(REVIEW_ID);
        System.out.println("#Test reviewModel : "+reviewModel);

        Assertions.assertEquals(reviewExpected,reviewModel);
    }

    @Test
    @Order(2)
    @DisplayName("testGetReviewByReviewIdNotFound")
    void testGetReviewByReviewIdNotFound(){
        System.out.println("#Test testGetReviewByReviewIdNotFound !!!");
        Assertions.assertThrows(ReviewNotFoundException.class , () -> {reviewServiceImpl.getReviewById(99999);});
    }

    @Test
    @Order(3)
    @DisplayName("testGet")
    void testGet(){
        //System.out.println("#Test testGetReviewByReviewIdNotFound !!!");
        Assertions.assertEquals(1,1);
    }
}
