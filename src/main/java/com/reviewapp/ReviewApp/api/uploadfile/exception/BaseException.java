package com.reviewapp.ReviewApp.api.uploadfile.exception;

public abstract class BaseException extends Exception {
    public BaseException(String errorCode) {
        super(errorCode);
    }
}
