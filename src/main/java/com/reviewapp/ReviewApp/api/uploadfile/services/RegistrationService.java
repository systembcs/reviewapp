package com.reviewapp.ReviewApp.api.uploadfile.services;

import com.reviewapp.ReviewApp.api.uploadfile.exception.BaseException;
import com.reviewapp.ReviewApp.api.uploadfile.model.TestModel;

import java.io.IOException;

public interface RegistrationService {
    public String registrationService(TestModel testModel) throws IOException, BaseException;
}
