package com.reviewapp.ReviewApp.api.uploadfile.services;

import com.reviewapp.ReviewApp.api.uploadfile.exception.BaseException;
import com.reviewapp.ReviewApp.api.uploadfile.model.UploadFileResponseDTO;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface FilesStorageService {

    public UploadFileResponseDTO save(String fileName , MultipartFile file) throws BaseException;

    Resource loadFileAsResource(String fileName) throws BaseException ;

    public void deleteAll() throws BaseException ;

}
