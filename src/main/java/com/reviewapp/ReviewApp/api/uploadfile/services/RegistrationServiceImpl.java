package com.reviewapp.ReviewApp.api.uploadfile.services;

import com.reviewapp.ReviewApp.api.uploadfile.exception.BaseException;
import com.reviewapp.ReviewApp.api.uploadfile.exception.RegistrationException;
import com.reviewapp.ReviewApp.api.uploadfile.model.TestModel;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class RegistrationServiceImpl implements RegistrationService {

    @Override
    public String registrationService(TestModel testModel) throws BaseException {
        if (testModel == null) {
            throw RegistrationException.requestNull();
        }
        //validate name
        if (Objects.isNull(testModel.getName()) || testModel.getName().isEmpty()) {
            throw RegistrationException.nameNull();
        }
        return "";
    }
}
