package com.reviewapp.ReviewApp.api.uploadfile.exception;

public class FileUploadException extends BaseException {
    public FileUploadException(String errorCode) {
        super("fileUploadException : " + errorCode);
    }

    //request = null
    public static FileUploadException fileError() {
        return new FileUploadException("file is null or empty");
    }

}
