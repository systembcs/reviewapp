package com.reviewapp.ReviewApp.api.uploadfile.model;

import lombok.Data;

@Data
public class TestUploadFileModel {
    private String fileName;
    //private String url;
}
