package com.reviewapp.ReviewApp.api.uploadfile.controller;


import com.reviewapp.ReviewApp.api.uploadfile.exception.BaseException;
import com.reviewapp.ReviewApp.api.uploadfile.model.UploadFileResponseDTO;
import com.reviewapp.ReviewApp.api.uploadfile.services.FilesStorageServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@Api(tags = "Test UploadFile Controller",description = "Controller for Test Upload File.")
@RequestMapping("/test")
public class TestUploadFileController {
    @Autowired
    private FilesStorageServiceImpl filesStorageServiceImpl;

    @ApiOperation(value = "uploadFile", notes = "uploadFile")
    @PostMapping(path="/upload",produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public UploadFileResponseDTO uploadFile(
            @ApiParam(value = "text of file", required = true) @Valid @RequestParam("text") String text,
            @ApiParam(value = "file upload", required = true) @Valid @RequestPart(value = "file", required = true) MultipartFile file
    ) throws BaseException {
        System.out.println("- text : "+text);
        System.out.println("- file : "+file.toString());

        UploadFileResponseDTO uploadFileResponseDTO = filesStorageServiceImpl.save(text,file);
        System.out.println("- uploadFileResponseDTO : "+uploadFileResponseDTO.toString());
        return uploadFileResponseDTO;
    }

    @ApiOperation(value = "uploadMultipleFiles", notes = "uploadMultipleFiles")
    @PostMapping(path = "/uploadMultipleFiles" ,produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public List<UploadFileResponseDTO> uploadMultipleFiles(
            @ApiParam(value = "file upload Multiple Files", required = true) @Valid @RequestPart ("files") MultipartFile[] files) throws BaseException{
        return Arrays.stream(files)
                .map(file -> {
                    try {
                        UploadFileResponseDTO uploadFileResponseDTO = filesStorageServiceImpl.save("text",file);
                        System.out.println("- uploadFileResponseDTO : "+uploadFileResponseDTO.toString());
                        return uploadFileResponseDTO;
                        //return uploadFileNoText(file);
                    } catch (BaseException e) {
                        throw new RuntimeException(e);
                    }
                })
                .collect(Collectors.toList());
    }

}
