package com.reviewapp.ReviewApp.api.uploadfile.controller;

import com.reviewapp.ReviewApp.api.uploadfile.exception.BaseException;
import com.reviewapp.ReviewApp.api.uploadfile.model.TestModel;
import com.reviewapp.ReviewApp.api.uploadfile.services.RegistrationServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/testRegister")
public class TestController {

    @Autowired
    private RegistrationServiceImpl registrationService;

    @GetMapping("/get")
    public List<TestModel> testGet() {
        List<TestModel> testModelList = new ArrayList<>();
        TestModel testModel = new TestModel();
        testModel.setName("Test1");
        testModel.setLastname("LastName1");
        testModelList.add(testModel);

        testModel.setName("Test2");
        testModel.setLastname("LastName2");
        testModelList.add(testModel);
        return testModelList;
    }

    @PostMapping("/registration")
    public ResponseEntity<String> registration(@RequestBody TestModel requestTestModel) throws BaseException {
        String response = registrationService.registrationService(requestTestModel);
        return ResponseEntity.ok(response);
    }
}
