package com.reviewapp.ReviewApp.api.reivew.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class ReviewDTO {
	
	private int reviewid;
	
	private String review;
	
	private String review_display;

}
