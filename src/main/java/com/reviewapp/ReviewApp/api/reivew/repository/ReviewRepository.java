package com.reviewapp.ReviewApp.api.reivew.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.reviewapp.ReviewApp.api.reivew.entity.Review;
import com.reviewapp.ReviewApp.api.reivew.entity.ReviewDictionary;

public interface ReviewRepository extends JpaRepository<Review, Integer>{
	List<Review> findByReviewContains(String review);

}