package com.reviewapp.ReviewApp.api.reivew.exception;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ReviewNotFoundException extends RuntimeException    {

	private static final long serialVersionUID = 4074216251766275683L;

	public ReviewNotFoundException(String message) {
		super(message);
	}
}
