package com.reviewapp.ReviewApp.api.reivew.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Entity
public class Review {
	
	@Id
	private int reviewid;
	
	private String review;
	
}
