package com.reviewapp.ReviewApp.api.reivew.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class ReviewRequst {

	@ApiModelProperty(position = 1, example = "test 123",required = true ,value = "review bla bla bla")
	@NotNull(message = "review is not null")
	@JsonProperty("review")
	private String review;
	
}
