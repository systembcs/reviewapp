package com.reviewapp.ReviewApp.api.reivew.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import com.reviewapp.ReviewApp.api.reivew.entity.ReviewRequst;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.reviewapp.ReviewApp.api.reivew.entity.Review;
import com.reviewapp.ReviewApp.api.reivew.entity.ReviewDTO;
import com.reviewapp.ReviewApp.api.reivew.entity.ReviewDictionary;
import com.reviewapp.ReviewApp.api.reivew.exception.ReviewNotFoundException;
import com.reviewapp.ReviewApp.api.reivew.repository.ReviewDictionaryRepository;
import com.reviewapp.ReviewApp.api.reivew.repository.ReviewRepository;

@Service
public class ReviewServiceImpl implements ReviewService {
	@Autowired
	private ReviewRepository review_repo;

	@Autowired
	private ReviewDictionaryRepository review_dic_repo;

	@Override
	public List<ReviewDTO> getReviewById(int id) {
		System.out.println(":: Review id - " + id);
		List<ReviewDTO> reviewDTOs = new ArrayList<ReviewDTO>();

		Optional<Review> reviewModel = review_repo.findById(id);
		//System.out.println("::reviewModel - " + reviewModel.get());
		if (reviewModel.isEmpty()) {
			throw new ReviewNotFoundException("Review id: " + id);
		} else {
			ReviewDTO reviewDto = new ReviewDTO();
			reviewDto.setReviewid(reviewModel.get().getReviewid());
			reviewDto.setReview(reviewModel.get().getReview());
			reviewDto.setReview_display(reviewModel.get().getReview());
			reviewDTOs.add(reviewDto);
		}

		return reviewDTOs;
	}

	@Override
	public List<ReviewDTO> getReviewByQuery(String query) {
		System.out.println(":: Review query - " + query);
		List<ReviewDTO> reviewDTOs = new ArrayList<ReviewDTO>();
		if (validateKeywordQuery(query)) {
			List<Review> review_list = review_repo.findByReviewContains(query);
			setReturnReviewDto(reviewDTOs, review_list, query);
		}
		if (reviewDTOs.isEmpty()) {
			throw new ReviewNotFoundException("Review not found : " + query);
		}
		return reviewDTOs;
	}

	/*
	 * @Override public ReviewRepository updateReviewById(int id, Review review) {
	 * Optional<Review> reviewModel = review_repo.findById(id);
	 * System.out.println("::reviewModel - " + reviewModel.get());
	 * 
	 * if (reviewModel.isEmpty() || review.getReview().isEmpty()) { throw new
	 * ReviewNotFoundException("Review id: " + id); } else { review.setReviewid(id);
	 * review.setReview(review.getReview().trim()); review_repo.save(review); }
	 * return review_repo; }
	 */
	
	@Transactional
	@Override
	public ReviewRepository updateReviewById(int id, ReviewRequst reviewRequst) {
		Optional<Review> reviewModel = review_repo.findById(id);
		if (reviewModel.isEmpty() || reviewRequst.getReview().isEmpty()) {
			throw new ReviewNotFoundException("Review id : " + id);
		}else {
			reviewModel.get().setReview(reviewRequst.getReview().trim());
		}
		return review_repo;
	}
	
	public boolean validateKeywordQuery(String query) {
		boolean valid = false;
		List<ReviewDictionary> keywordList20000 = review_dic_repo.findKeywordLimit20000();
		System.out.println(":: keywordList20000 - " + keywordList20000.size());
		List<ReviewDictionary> keywordList = keywordList20000.stream()
				.filter(item -> item.getKeyword().equalsIgnoreCase(query)).collect(Collectors.toList());
		System.out.println(":: keywordList - " + keywordList.size());

		if (keywordList.size() > 0) {
			valid = true;
		}
		return valid;
	}

	public List<ReviewDTO> setReturnReviewDto(List<ReviewDTO> reviewDTOs, List<Review> review_list, String query) {
		System.out.println(":: review_list - " + review_list.size());
		if (review_list.size() > 0) {
			String highlightKeyword = "<keyword>".concat(query).concat("</keyword>");
			for(Review review : review_list){
				ReviewDTO reviewDto = new ReviewDTO();
				reviewDto.setReviewid(review.getReviewid());
				reviewDto.setReview(review.getReview());
				reviewDto.setReview_display(review.getReview().replaceAll(query, highlightKeyword));
				System.out.println("--review : " + review.getReview());

				reviewDTOs.add(reviewDto);
			}
		}
		return reviewDTOs;
	}

}
