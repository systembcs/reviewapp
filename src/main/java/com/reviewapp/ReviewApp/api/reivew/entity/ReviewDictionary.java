package com.reviewapp.ReviewApp.api.reivew.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;


@Data
@Entity
@Table(name="review_dictionary")
public class ReviewDictionary {
	@Id
	private Integer id;
	
	private String keyword;
}
