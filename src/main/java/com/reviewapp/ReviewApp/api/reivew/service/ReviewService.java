package com.reviewapp.ReviewApp.api.reivew.service;

import java.util.List;

import com.reviewapp.ReviewApp.api.reivew.entity.ReviewRequst;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import com.reviewapp.ReviewApp.api.reivew.entity.Review;
import com.reviewapp.ReviewApp.api.reivew.entity.ReviewDTO;
import com.reviewapp.ReviewApp.api.reivew.repository.ReviewRepository;

public interface ReviewService  {

	public List<ReviewDTO> getReviewById(int id);
	
	public List<ReviewDTO> getReviewByQuery(String query);
	
	public ReviewRepository updateReviewById(int id, ReviewRequst reviewRequst);
}
