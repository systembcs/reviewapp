package com.reviewapp.ReviewApp.api.reivew.controller;

import java.util.List;

import com.reviewapp.ReviewApp.api.reivew.entity.ReviewRequst;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.reviewapp.ReviewApp.api.reivew.entity.Review;
import com.reviewapp.ReviewApp.api.reivew.entity.ReviewDTO;
import com.reviewapp.ReviewApp.api.reivew.service.ReviewService;

import lombok.Setter;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.validation.Valid;

//@EnableSwagger2
@RestController
@Api(tags = "Review Controller",description = "Controller for review API")
public class ReviewController {

	@Autowired
	public ReviewService reviewService;


	@ApiOperation(value = "Review by ID", notes = "Inquiry Review by ID")
	@GetMapping("/reviews/{id}")
	public List<ReviewDTO> getReviewById(@ApiParam(value = "Unique ID for query review", required = true) @PathVariable int id) {
		return reviewService.getReviewById(id);
	}

	@ApiOperation(value = "Review by Query", notes = "Inquiry Review by Query")
	@GetMapping("/reviews")
	public List<ReviewDTO> getReviewByQuery(@ApiParam(value = "{query} for query review", required = true) @RequestParam String query) {
		// return null;
		return reviewService.getReviewByQuery(query);

	}

	@ApiOperation(value = "Update review", notes = "Update review by ID")
	@PutMapping("/reviews/{id}")
	public ResponseEntity<?> updateReviewById(@ApiParam(value = "Unique {ID} for query review", required = true) @PathVariable int id
											,@ApiParam(value = "Data for update review",required = true ) @Valid @RequestBody ReviewRequst reviewRequst) {
		reviewService.updateReviewById(id, reviewRequst);
		return ResponseEntity.ok("Update Success!!!");
	}
}
