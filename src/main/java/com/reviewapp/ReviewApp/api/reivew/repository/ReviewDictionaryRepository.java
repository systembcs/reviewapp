package com.reviewapp.ReviewApp.api.reivew.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.reviewapp.ReviewApp.api.reivew.entity.ReviewDictionary;

public interface ReviewDictionaryRepository extends JpaRepository<ReviewDictionary, Integer>{
	List<ReviewDictionary> findByKeyword(String keyword);
	
	@Query(nativeQuery = true,value = "SELECT * FROM review_dictionary Limit 20000")
	List<ReviewDictionary> findKeywordLimit20000();
	
}
